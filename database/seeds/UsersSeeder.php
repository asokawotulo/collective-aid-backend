<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Create Admin
		$login_id = factory(App\Models\Login::class)->create([
			'email' => 'admin@example.com',
			'role_id' => Role::where('role', 'admin')->pluck('id')->first()
		])->id;
		factory(App\Models\Admin::class)->create([
			'login_id' => $login_id
		]);

		// Create Verifier
		$login_id = factory(App\Models\Login::class)->create([
			'email' => 'verifier@example.com',
			'role_id' => Role::where('role', 'verifier')->pluck('id')->first()
		])->id;
		factory(App\Models\Verifier::class)->create([
			'login_id' => $login_id
		]);

		// Create Volunteer
		$login_id = factory(App\Models\Login::class)->create([
			'email' => 'volunteer@example.com',
			'role_id' => Role::where('role', 'volunteer')->pluck('id')->first()
		])->id;
		factory(App\Models\Volunteer::class)->create([
			'login_id' => $login_id
		]);

		// Create Provider
		$login_id = factory(App\Models\Login::class)->create([
			'email' => 'provider@example.com',
			'role_id' => Role::where('role', 'provider')->pluck('id')->first()
		])->id;
		factory(App\Models\Provider::class)->create([
			'login_id' => $login_id
		]);
    }
}
