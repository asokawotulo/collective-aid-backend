<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RolesTableSeeder::class);
        $this->call(UsersSeeder::class);
        $this->call(ServiceTableSeeder::class);
        $this->call(StatusTableSeeder::class);
        // $this->call(LoginsTableSeeder::class);
    }
}
