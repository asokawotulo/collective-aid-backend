<?php

use Illuminate\Database\Seeder;

class ServiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$services = [
    		'Education',
			'Environmental Aid',
			'Health & Well Being',
			'Social, Animal & Human Rights',
			'Labor Work',
			'Others',
    	];

    	foreach ($services as $service) {
	    	factory(App\Models\Service::class)->create([
	    		'service' => $service
	    	]);
    	}
    }
}
