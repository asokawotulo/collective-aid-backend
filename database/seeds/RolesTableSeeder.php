<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$roles = ['admin', 'verifier', 'provider', 'volunteer'];
    	foreach( $roles as $role ) {
			factory(App\Models\Role::class)->create([
				'role' => $role
			]);
		}
    }
}
