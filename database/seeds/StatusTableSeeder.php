<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$statuses = ['Pending', 'Unapproved', 'Approved', 'In Progress', 'Complete', 'Cancelled'];

    	foreach ($statuses as $status) {
	        factory(App\Models\Status::class)->create([
	        	'status' => $status
	        ]);
    	}
    }
}
