<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Login::class, function (Faker $faker) {
    return [
        'name' => "$faker->firstName $faker->lastName",
		'username' => $faker->userName(),
		'email' => $faker->safeEmail(),
		'dob' => $faker->date($format = 'Y-m-d', $max = '1/1/2004'),
		'description' => $faker->sentence($nbWords = 10),
		'picture' => 'storage/profiles/default.jpg',
		'password' => '$2y$10$EnynpD.MlqNZXyV3XrOSpeWsZ/c6sKuGBBgyJ/3EzRF/sQEZ6RbZm', //password
		'email_verified_at' => now(),
    ];
});
