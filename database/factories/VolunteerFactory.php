<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Volunteer::class, function (Faker $faker) {
    return [
		'identification' => '/uploads/identification/identification.jpg',
		'cv' => 'uploads/cv/cv.jpg',
		'cv_verified' => 1
    ];
});
