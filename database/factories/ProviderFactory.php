<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Provider::class, function (Faker $faker) {
    return [
		'identification' => '/uploads/identification/identification.jpg'
    ];
});
