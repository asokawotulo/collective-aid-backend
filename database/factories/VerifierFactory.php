<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Verifier::class, function (Faker $faker) {
    return [
		'identification' => '/uploads/identification/identification.jpg'
    ];
});
