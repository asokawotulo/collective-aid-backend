<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Verifier extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login_id', 'identification'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * Login associated with model
     * 
     * @return hasMany
     */
    public function login()
    {
    	return $this->belongsTo('App\Models\Login');
    }
}
