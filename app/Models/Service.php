<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'service'
    ];

    /**
     * Location services associated with servie.
     * 
     * @return hasMany
     */
    public function location_service()
    {
    	return $this->hasMany('App\Models\Login');
    }
}
