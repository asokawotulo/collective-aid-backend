<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name', 'description',
    	'location', 'capacity',
    	'verified', 'date_from', 'date_to',
    	'provider_id',
    ];

    /**
     * Provider associated with event
     * 
     * @return belongsTo
     */
    public function provider()
    {
    	return $this->belongsTo('App\Models\Provider');
    }

    /**
     * Pictures associated with model
     * 
     * @return hasMany
     */
    public function pictures()
    {
        return $this->hasMany('App\Models\EventPicture');
    }

    /**
     * Services associated with model
     * 
     * @return hasMany
     */
    public function services()
    {
        return $this->hasMany('App\Models\EventService');
    }
}
