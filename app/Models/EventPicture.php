<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventPicture extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'event_id',
    	'picture',
    ];

    /**
     * Event associated with model
     * 
     * @return belongsTo
     */
    public function event()
    {
    	return $this->belongsTo('App\Models\Event');
    }
}
