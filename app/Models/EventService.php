<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventService extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'event_id',
    	'service_id',
    ];

    /**
     * Location associated with model
     * 
     * @return belongsTo
     */
    public function event()
    {
    	return $this->belongsTo('App\Models\Event');
    }

    /**
     * Service associated with model
     * 
     * @return belongsTo
     */
    public function service()
    {
    	return $this->belongsTo('App\Models\Service');
    }
}
