<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'event_id', 'status_id', 'volunteer_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * Volunteer associated with model
     * 
     * @return belongsTo
     */
    public function volunteer()
    {
    	return $this->belongsTo('App\Models\Volunteer');
    }

    /**
     * Status associated with model
     * 
     * @return belongsTo
     */
    public function status()
    {
    	return $this->belongsTo('App\Models\Status');
    }

    /**
     * Event associated with model
     * 
     * @return belongsTo
     */
    public function event()
    {
    	return $this->belongsTo('App\Models\Event');
    }
}
