<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Login extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'name', 'username', 'email',
        'description', 'picture',
        'password',
        'role_id', 'dob',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Role associated with model
     * 
     * @return belongsTo
     */
    public function role()
    {
    	return $this->belongsTo('App\Models\Role');
    }

    /**
     * Detail associated with login based on role.
     * 
     * @return hasOne
     */
    public function detail()
    {
        $role = $this->role->role;

        switch ($role) {
            case 'admin':
                return $this->hasOne('App\Models\Admin');

            case 'verifier':
                return $this->hasOne('App\Models\Verifier');

            case 'provider':
                return $this->hasOne('App\Models\Provider');

            case 'volunteer':
                return $this->hasOne('App\Models\Volunteer');
        }
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new \App\Notifications\EmailVerification);
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
