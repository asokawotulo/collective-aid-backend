<?php

namespace App\Http\Controllers;

use App\Models\Watchlist;
use App\Models\Event;
use Illuminate\Http\Request;

class WatchlistController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['jwt.auth', 'role:volunteer']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $watchlist = Watchlist::where('volunteer_id', auth()->user()->id)->get();

        return response()->json($watchlist, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Event $event)
    {
        $watchlist = Watchlist::create([
            'event_id' => $event->id,
            'volunteer_id' => auth()->user()->detail->id,
        ]);

        return response()->json($watchlist, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Watchlist  $watchlist
     * @return \Illuminate\Http\Response
     */
    public function show(Watchlist $watchlist)
    {
        $watchlist->event;
        $watchlist->volunteer;

        return response()->json($watchlist, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Watchlist  $watchlist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Watchlist $watchlist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Watchlist  $watchlist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Watchlist $watchlist)
    {
        $watchlist_old = $watchlist;
        $watchlist_old->event;
        $watchlist_old->volunteer;

        $watchlist->delete();

        return response()->json($watchlist_old, 202);
    }
}
