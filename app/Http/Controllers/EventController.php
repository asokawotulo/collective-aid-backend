<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Event;
use App\Models\EventService;
use App\Models\EventPicture;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['jwt.auth', 'role:provider,admin'])->only([
            'store', 'store_image', 'update', 'destroy', 'destroy_image'
        ]);

        $this->middleware(['jwt.auth', 'role:verifier,admin'])->only([
            'verify'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $event = Event::with(['pictures','services'])->where('verified', 1)->get();
        if (auth()->user() && auth()->user()->role->role == 'provider') {
            $event = Event::with(['pictures','services'])->where('provider_id', auth()->user()->detail->id)->get();
        }
        return response()->json($event);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate($this->storeRules());

        $event = Event::create([
            'name' => $request->name,
            'description' => $request->description,
            'location' => $request->location,
            'capacity' => $request->capacity,
            'date_from' => date('Y-m-d', strtotime($request->date_from)),
            'date_to' => date('Y-m-d', strtotime($request->date_to)),
            'provider_id' => auth()->user()->detail->id,
        ]);
        
        foreach ($request->services as $service) {
            EventService::create([
                'event_id' => $event->id,
                'service_id' => $service,
            ]);
        }
        foreach ($request->file('pictures') as $picture) {
            $picture_path = Storage::url(Storage::putFile('public/events', $picture));
            EventPicture::create([
                'event_id' => $event->id,
                'picture' => $picture_path,
            ]);
        }
        $event->pictures;
        $event->services;
        return response()->json($event);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_image(Request $request, Event $event)
    {
        foreach ($request->file('pictures') as $picture) {
            $picture_path = Storage::url(Storage::putFile('public/events', $request->file('picture')));
            EventPicture::create([
                'event_id' => $event->id,
                'picture' => $picture_path,
            ]);
        }
        $event->pictures;
        $event->services;
        return response()->json($event);
    }

    /**
     * Display the filtered resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request)
    {
        $event_ids = $request->services ? EventService::whereIn('id', $request->services)->pluck('event_id') : EventService::all()->pluck('event_id');
        $events = Event::with(['pictures', 'services'])->whereIn('id', $event_ids)->get();

        return response()->json($events);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        $event->pictures;
        $event->services;
        return response()->json($event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $event->fill($request->all());
        $event->save();

        if($request->services){
            foreach ($event->services as $service) {
                $service->delete();
            }
            foreach ($request->services as $service) {
                EventService::create([
                    'event_id' => $event->id,
                    'service_id' => $service,
                ]);
            }
        }

        $event->pictures;
        $event->services;
        return response()->json($event);
    }

    /**
     * Verfify Event
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function verify(Event $event)
    {
        $event->verified = 1;
        $event->save();

        $event->pictures;
        $event->services;

        return response()->json($event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        $eventold = $event;
        $eventold->pictures;
        $eventold->services;

        foreach ($event->pictures as $picture) {
            Storage::delete(str_replace('storage', 'public', $picture->picture));
            $picture->delete();
        }

        foreach ($event->services as $service) {
            $service->delete();
        }

        $event->delete();

        return response()->json([
            'event' => $eventold,
            'message' => 'Successfully deleted'
        ], 204);
    }

    /**
     * Remove the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy_image(Request $request, Event $event, EventPicture $picture)
    {
        $picture_old = $picture;

        if(!$picture->event_id == $event->id) {
            return response()->json(['message' => 'Error picture does not exist in this event.', 400]);
        }

        Storage::delete(str_replace('storage', 'public', $picture_old->picture));
        $picture->delete();

        return response()->json([
            'picture' => $picture_old,
            'message' => 'Successfully deleted'
        ], 204);
    }

    /*
    |--------------------------------------------------------------------------
    | Validation
    |--------------------------------------------------------------------------
    */
   
    /**
     * Define rules for validating request
     * 
     * @return array
     */
    public function storeRules()
    {
        $rules = [
            'name'        => ['required', 'string', 'max:255'],
            'description' => ['required', 'string', 'max:255'],
            'location'    => ['required', 'string', 'max:255'],
            'capacity'    => ['required', 'numeric'],
            'date_from'   => ['required', 'date', 'after:today'],
            'date_to'     => ['required', 'date', 'after_or_equal:date_from'],
            'pictures'    => ['required'],
            'pictures.*'  => ['mimes:jpg,png,jpeg'],
            'services'    => ['required'],
            'services.*'  => ['exists:services,id'],
        ];

        return $rules;
    }
}
