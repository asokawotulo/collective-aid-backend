<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Event;
use Illuminate\Http\Request;

class BookingController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['jwt.auth', 'role:admin,provider,volunteer'])->except(['store']);
        $this->middleware(['jwt.auth', 'role:volunteer'])->only(['store']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = auth()->user()->role->role;

        if($role == 'admin') {
            $bookings == Booking::all();
        } else if($role == 'provider') {
            $bookings = Booking::whereHas('event', function($event){
                $event->where('provider_id', auth()->user()->detail->id);
            });
        } else if($role == 'volunteer') {
            $bookings = Booking::where('volunteer_id', auth()->user()->detail->id)->get();
        }

        return response()->json($bookings, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Event $event)
    {
        $bookings = Booking::create([
            'event_id' => $event->id,
            'status_id' => 1,
            'volunteer_id' => auth()->user()->detail->id,
        ]);

        return response()->json($bookings, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        $booking->event;
        $booking->status;
        $booking->volunteer;

        return response()->json($booking, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Booking  $booking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        $booking_old = $booking;
        $booking_old->event;
        $booking_old->status;
        $booking_old->volunteer;

        $booking->delete();

        return response()->json($booking_old, 202);
    }
}
