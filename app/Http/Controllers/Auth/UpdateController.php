<?php

namespace App\Http\Controllers\Auth;

use App\Models\Login;
use App\Models\Volunteer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class UpdateController extends Controller
{
	/* Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
        // $this->middleware('auth:api');
	}

	public function edit(Request $request)
	{
        $validator = $this->validator($request);
        if ( $validator->fails() ) return response()->json($validator->messages(), 400);

        $user = $this->update($request);

        return response()->json($user, 201);
	}

	public function validator(Request $request)
	{
		return Validator::make($request->all(), [
			'name' => ['string', 'max:255'],
			'username' => ['string', 'max:255'],
			'email' => ['string', 'email', 'max:255'],
			'description' => ['string', 'max:255'],
			'password' => ['string', 'min:8', 'confirmed'],
			'dob' => ['date', 'before:1/1/2004'],
			'picture' => ['mimes:jpg,png,jpeg', 'max:2048'],
			'identification' => ['mimes:jpg,png,jpeg', 'max:2048'],
			'cv' => ['mimes:jpg,png,jpeg', 'max:2048'], // To do: add PDF mime
        ]);
	}

	public function update(Request $request)
	{
		$request_data = $request->all();

		if($request->file('picture')){
			Storage::delete(str_replace('storage', 'public', auth()->user()->picture));
			$request_data['picture'] = Storage::url(Storage::putFile('public/picture', $request->file('picture')));
		}
		if($request->file('identification')){
			Storage::delete(str_replace('storage', 'public', auth()->user()->identification));
			$request_data['identification'] = Storage::url(Storage::putFile('public/identification', $request->file('identification')));
		}
		if($request->file('cv')){
			Storage::delete(str_replace('storage', 'public', auth()->user()->cv));
			$request_data['cv'] = Storage::url(Storage::putFile('public/cv', $request->file('cv')));
		}
		if($request->password){
			$request_data['password'] = Hash::make($request->password);
		}
		if($request->dob){
			$request_data['dob'] = date('Y-m-d', strtotime($request->input("dob")));
		}

		$user = auth()->user();
		$user->fill($request_data);
		$user->detail->fill($request_data);
		$user->save();

		auth()->user()->role;
		auth()->user()->detail;
		return response()->json(auth()->user(), 200);
	}
}
