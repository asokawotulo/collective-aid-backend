<?php

namespace App\Http\Controllers\Auth;

use App\Models\Role;
use App\Models\Login;
use App\Models\Volunteer;
use \Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'email/verify';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = $this->validator($request);
        if ( $validator->fails() ) return response()->json($validator->messages(), 400);

        event(new Registered($user = $this->create($request)));

        $user->role;
        $user->detail;
        return response()->json($user, 200);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  Request $request
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(Request $request)
    {
        return Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255', 'unique:logins'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:logins'],
            'description' => ['string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'dob' => ['required', 'date', 'before:1/1/2004'],
            'picture' => ['required', 'mimes:jpg,png,jpeg', 'max:2048'],
            'identification' => ['required', 'mimes:jpg,png,jpeg', 'max:2048'],
            'cv' => ['mimes:jpg,png,jpeg', 'max:2048'], // To do: add PDF mime
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  Request $request
     * @return \App\User
     */
    protected function create(Request $request)
    {
        // File upload
        $photo_path = Storage::url(Storage::putFile('public/pictures', $request->file('picture')));
        $id_path = Storage::url(Storage::putFile('public/identifications', $request->file('identification')));
        $cv_path = $request->file('cv') ? Storage::url(Storage::putFile('public/cvs', $request->file('cv'))) : NULL;
        
        // Create login row 
        $login = Login::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'description' => $request->description,
            'password' => Hash::make($request->password),
            'dob' => date('Y-m-d', strtotime($request->dob)),
            'picture' => $photo_path,
            'role_id' => Role::where('role', 'volunteer')->pluck('id')->first(),
        ]);

        // Create volunteer row
        $volunteer = Volunteer::create([
            'login_id' => $login->id,
            'identification' => $id_path,
            'cv' => $cv_path
        ]);
        
        return $login;
    }
}
