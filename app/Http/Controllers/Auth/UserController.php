<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
	/* Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth:api');
		$this->middleware('jwt.auth');
	}

	/**
	 * Refresh a token.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function refresh()
	{
		return $this->respondWithToken(auth()->refresh());
	}

	/**
	 * Get the authenticated User.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function user()
	{
		auth()->user()->role;
		auth()->user()->detail;
		return response()->json(auth()->user(), 200);
	}

	/**
	 * Get the verified state of User.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function verified()
	{
		return response()->json([
			'message' => auth()->user()->hasVerifiedEmail()
		], 200);
	}

	/**
	 * Log the user out (Invalidate the token).
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function logout()
	{
		auth()->logout();

		return response()->json(['message' => 'Successfully logged out']);
	}

	/**
	 * Delete user data.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function delete()
	{
		$userold = auth()->user();
		$userdetail = auth()->user()->detail;
		Storage::delete(str_replace('storage', 'public', $userold->picture));
		Storage::delete(str_replace('storage', 'public', $userold->identification));
		!$userold->cv ?: Storage::delete(str_replace('storage', 'public', $userold->cv));

		auth()->user()->detail->delete();
		auth()->user()->delete();

		return response()->json([
			'user' => $userold,
			'message' => 'Successfully deleted'
		], 202);
	}

	/**
	 * Get the token array structure.
	 *
	 * @param  string $token
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	protected function respondWithToken($token)
	{
		return response()->json([
			'access_token' => $token,
			'token_type' => 'bearer',
			'expires_in' => auth()->factory()->getTTL() * 60
		]);
	}
}
