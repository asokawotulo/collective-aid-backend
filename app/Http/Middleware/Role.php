<?php

namespace App\Http\Middleware;

use Closure;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, ...$roles)
    {
        if(in_array(auth()->user()->role->role, $roles)){
            return $next($request);
        }
        return response()->json([
            "message" => "",
            "message" => "Error not a(n) " . implode(' or ', $roles) . "."
        ], 401);
    }
}
