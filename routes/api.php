<?php

use App\Models\Login;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
| User Actions
|--------------------------------------------------------------------------
*/


Route::prefix('auth')->group(function(){
	// Registration
	Route::post('register', 'Auth\RegisterController@register');

	// Login
	Route::post('login', 'Auth\LoginController@login');

	// Logout
	Route::post('logout', 'Auth\UserController@logout');

	// Refresh Token
	Route::post('refresh', 'Auth\UserController@refresh');

	// User Data
	Route::get('user', 'Auth\UserController@user');

	// Delete User
	Route::delete('delete', 'Auth\UserController@delete');

	// User verified state
	Route::get('verified', 'Auth\UserController@verified');

	// Update user information
	Route::put('update', 'Auth\UpdateController@update');
});

// Statuses
Route::apiResource('status', 'StatusController');

// Services
Route::apiResource('service', 'ServiceController');

// Events
Route::apiResource('event', 'EventController');

// Filter Event
Route::post('event/filter', 'EventController@filter')->name('event.filter');

// Verify Event
Route::post('event/{event}/verify', 'EventController@verify')->name('event.verify');

// Event Picture
Route::post('event/{event}', 'EventController@store_image')->name('event.store.image');
Route::delete('event/{event}/{picture}', 'EventController@destroy_image')->name('event.destroy.image');

// Watchlist
Route::apiResource('watchlist', 'WatchlistController')->except(['store', 'update']);

// Watchlist Store
Route::post('watchlist/{event}', 'WatchlistController@store')->name('watchlist.store');

// Booking
Route::apiResource('booking', 'BookingController')->except(['store', 'update']);

// Booking Store
Route::post('booking/{event}', 'BookingController@store')->name('booking.store');

// Provider
Route::apiResource('provider', 'ProviderController')->only(['index', 'show']);

/*
|--------------------------------------------------------------------------
| Email Verification
|--------------------------------------------------------------------------
*/

Route::prefix('email')->group(function(){
	// Verify email
	Route::get('verify/{id}', 'Auth\VerificationController@verify')->name('verification.verify');

	// Resend verification email
	Route::get('resend', 'Auth\VerificationController@resend')->name('verification.resend');
});