@component('mail::message')
# Thank you for registering.

But before click the button below to verify your email address.

@component('mail::button', ['url' => $actionUrl])
{{ $actionText }}
@endcomponent

If you did not create an account, no further action is required.

Thank you and good luck

Best Regards,<br>
The Collective Aid Team

@slot('subcopy')
@lang(
    "If you’re having trouble clicking the \":actionText\" button, copy and paste the URL below\n".
    'into your web browser: [:actionURL](:actionURL)',
    [
        'actionText' => $actionText,
        'actionURL' => $actionUrl,
    ]
)
@endslot

@endcomponent